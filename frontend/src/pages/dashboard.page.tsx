import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [page, setPage] = useState<number>(1);

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers(page);
      console.log(result.data);
      setUsers(result.data);
      setLoading(false);
    };

    fetchData();
  }, [page]);

  return (
    <div style={{ paddingTop: "30px", display: 'flex' }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
        {Array.from({ length: 10 },  (_, idx) => idx).map((index) => {
          return (
            <button
              key={index+1}
              onClick={() => {
                setPage(index+1);
              }}
              disabled={index+1 === page}
            >
              {index+1}
            </button>
          );
        })}
      </div>
    </div>
  );
};
